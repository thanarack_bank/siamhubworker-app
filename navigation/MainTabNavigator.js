import React from "react"
import { Platform } from "react-native"
import { createDrawerNavigator } from "react-navigation"
import HomeScreen from "../screens/HomeScreen"
import SettingsScreen from "../screens/SettingsScreen"
import ShowCategory from "../screens/ShowCategory"
import SlideMenu from "../components/Headers/SlideMenu"
import { Icon } from "native-base"
import { layoutWidthPercentage } from "../constants/Layout"
import Config from "../constants/Config"
import PostScreen from "./../screens/PostScreen"
import AboutScreen from "./../screens/AboutScreen"

const configDrawer = {
  initialRouteName: "Home",
  contentComponent: SlideMenu,
  drawerWidth: layoutWidthPercentage(Config.reduceSlideMenu),
  navigationOptions: {
    header: null
  }
}

const configRoutes = {
  Home: {
    screen: HomeScreen
  },
  Post: {
    screen: PostScreen
  },
  Category: {
    screen: ShowCategory
  },
  Setting: {
    screen: SettingsScreen
  },
  About: {
    screen: AboutScreen
  },
  Help: {
    screen: SettingsScreen
  }
}

export default createDrawerNavigator(configRoutes, configDrawer)
