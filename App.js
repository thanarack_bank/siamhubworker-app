import React from "react"
import { Platform, StatusBar, StyleSheet, View } from "react-native"
import { AppLoading, Asset, Font, Icon } from "expo"
import AppNavigator from "./navigation/AppNavigator"
import Colors from "./constants/Colors"
import { AdMobBanner } from "expo"

export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  }

  /*componentDidMount() {

  }*/

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      )
    } else {
      return (
        <View style={styles.container}>
          {/*Platform.OS === "ios" && <StatusBar barStyle="default" />*/}
          <View style={styles.statusBar} />
          <AppNavigator />
          <View style={styles.banner_ads}>
            <AdMobBanner
              bannerSize="smartBannerLandscape"
              adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
              testDeviceID="EMULATOR"
              // onDidFailToReceiveAdWithError={this.bannerError}
            />
          </View>
        </View>
      )
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require("./assets/images/robot-dev.png"),
        require("./assets/images/robot-prod.png")
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf"),
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
      })
    ])
  }

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error)
  }

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
    /*...Platform.select({
      android: {
        marginTop: StatusBar.currentHeight
      }
    })*/
  },
  statusBar: {
    backgroundColor: Colors.tabBar,
    height: StatusBar.currentHeight
  }
})
