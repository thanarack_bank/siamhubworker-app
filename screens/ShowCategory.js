import React, { Component } from "react"
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity
} from "react-native"
// import { WebBrowser } from "expo";
import { Container } from "native-base"
import HeaderMenu from "../components/Headers/HeaderMenu"
import PostList from "../components/Post/PostList"

export default class ShowCategory extends Component {
  render() {
    const navigation = this.props.navigation
    return (
      <Container>
        <HeaderMenu {...this.props} title={navigation.state.params.name} hasTabs />
        <PostList {...this.props} />
      </Container>
    )
  }
}
