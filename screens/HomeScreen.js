import React, { Component } from "react"
import { StyleSheet } from "react-native"
// import { WebBrowser } from "expo";
import { Container, Tabs, Tab } from "native-base"
import HeaderMenu from "../components/Headers/HeaderMenu"
import PostList from "../components/Post/PostList"
import api from "./../service/api"

export default class HomeScreen extends Component {
  componentDidMount() {
    this.getMoviesFromApi()
  }

  async getMoviesFromApi() {
    try {
      const dataApi = {
        path: "https://facebook.github.io/react-native/movies.json",
        method: "GET"
      }
      let response = await api(dataApi)
      // return responseJson.movies
      console.log(response)
    } catch (error) {
      console.error(error)
    }
  }

  render() {
    return (
      <Container>
        <HeaderMenu {...this.props} title="งานล่าสุด" hasTabs />
        <PostList {...this.props} />
        {/* <Tabs tabBarPosition="top">
          <Tab
            heading="วันที่"
            tabStyle={styles.tab}
            activeTabStyle={styles.tab}
            textStyle={styles.tabText}
            activeTextStyle={styles.activeTabText}
          >
            <PostList {...this.props} />
          </Tab>
          <Tab
            heading="ยอดนิยม"
            tabStyle={styles.tab}
            activeTabStyle={styles.tab}
            textStyle={styles.tabText}
            activeTextStyle={styles.activeTabText}
          >
            <PostList {...this.props} />
          </Tab>
          <Tab
            heading="เงินเดือน"
            tabStyle={styles.tab}
            activeTabStyle={styles.tab}
            textStyle={styles.tabText}
            activeTextStyle={styles.activeTabText}
          >
            <PostList {...this.props} />
          </Tab>
        </Tabs> */}
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  tab: {
    backgroundColor: "#1EA375"
  },
  tabText: {
    color: "#7ee7c2",
    fontSize: 16
  },
  activeTabText: {
    color: "#FFFFFF",
    fontSize: 16
  }
})
