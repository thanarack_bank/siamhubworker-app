import React from "react"
// import { ExpoConfigView } from "@expo/samples";
import { AdMobBanner } from "expo"
import {
  Container,
  Card,
  CardItem,
  View,
  Text,
  Body,
  Icon,
  Button
} from "native-base"
import { ScrollView } from "react-native"
import HeaderMenu from "../components/Headers/HeaderMenu"
import styles from "../styles/styles"

export default class PostScreen extends React.Component {
  render() {
    console.log(this.props)
    return (
      <Container>
        <HeaderMenu
          {...this.props}
          title="สำนักงานปลัดกระทรวงการคลัง รับสมัครเป็นพนักงานรากชาร 3 อัตรา"
        />
        <ScrollView>
          <View style={styles.postScreen}>
            <View style={styles.cardPadding}>
              <Card style={styles.card}>
                <CardItem>
                  <Body style={styles.cardBottom}>
                    <View style={styles.labelInfo}>
                      <Text
                        style={[
                          styles.labelText,
                          styles.labelColor,
                          styles.headTitle,
                          styles.headColor
                        ]}
                      >
                        สำนักงานปลัดกระทรวงการคลัง รับสมัครเป็นพนักงานรากชาร 3
                        อัตรา
                      </Text>
                    </View>
                    <View style={styles.labelInfo}>
                      <Icon
                        style={[styles.labelIcon, styles.labelColor]}
                        type="MaterialCommunityIcons"
                        name="calendar-range"
                      />
                      <Text style={[styles.labelText, styles.labelColor]}>
                        20 ส.ค 2562
                      </Text>
                    </View>
                    <View style={styles.labelInfo}>
                      <Icon
                        style={[styles.labelIcon, styles.labelColor]}
                        type="MaterialCommunityIcons"
                        name="domain"
                      />
                      <Text style={[styles.labelText, styles.labelColor]}>
                        กระทรวงการคลัง
                      </Text>
                    </View>
                    <View style={styles.labelInfo}>
                      <Icon
                        style={[styles.labelIcon, styles.labelColor]}
                        type="MaterialCommunityIcons"
                        name="map-marker"
                      />
                      <Text style={[styles.labelText, styles.labelColor]}>
                        ยโสธร, ขอนแก่น
                      </Text>
                    </View>
                    <View style={styles.labelInfo}>
                      <Icon
                        style={[styles.labelIcon, styles.labelColor]}
                        type="MaterialCommunityIcons"
                        name="cash-usd"
                      />
                      <Text style={[styles.labelText, styles.labelColor]}>
                        30,000 - 50,000
                      </Text>
                    </View>
                    <View style={styles.labelInfo}>
                      <Icon
                        style={[styles.labelIcon, styles.labelColor]}
                        type="MaterialCommunityIcons"
                        name="briefcase"
                      />
                      <Text style={[styles.labelText, styles.labelColor]}>
                        โปรแกรมเมอร์, ไอที
                      </Text>
                    </View>
                    <View style={styles.banner_ads}>
                      <AdMobBanner
                        bannerSize="largeBanner"
                        adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
                        testDeviceID="EMULATOR"
                        // onDidFailToReceiveAdWithError={this.bannerError}
                      />
                    </View>
                    <View style={styles.row}>
                      <Text style={[styles.detilsText]}>รายละเอียด</Text>
                    </View>
                    <View style={styles.row}>
                      <Text style={styles.contentText}>
                        ประกาศผลสอบครูผู้ช่วย
                        แข่งขันเพื่อบรรจุและแต่งตั้งบุคคลเข้ารับราชการเป็นข้าราชการครูและบุคลากรทางการศึกษาตำแหน่งครูผู้ช่วยกศจ.ทุกจังหวัดทั่วประเทศ
                        ครั้ง 1/2559 บัดนี้
                        การดำเนินการสอบแข่งขันเพื่อบรรจุและแต่งตั้งบุคคลเข้ารับราชการเป็นข้าราชการครูและบุคคลากรทางการศึกษา
                        ตำแหน่งครูผู้ช่วย ครั้งที่ 1 ปี พ.ศ. 2559
                        สังกัดสำนักงานศึกษาธิการแต่ละจังหวัด ได้เสร็จสิ้งแล้ว
                        จึงประกาศรายชื่อผู้สอบแข่งขันและขึ้นบัญชีผู้สอบแข่งขันได้
                        ดังรายละเอียดต่อไปนี้
                        ทางทีมงานสอบ.คอมจะพยายามอัพเดทการประกาศผลสอบครูผู้ช่วยให้เร็วที่สุดและถ้ามีประกาศ
                        เรียกบรรจุครูผู้ช่วยเพิ่มเติมจะนำมาลงในประกาศให้ใช่กันหรือท่านสามารถแจ้งลิงค์ประกาศมาได้ทาง
                        Fan-Page และทางไลน์ @RRW1933C (มี@ด้านหน้าด้วย) เว็บไซต์
                        สอบ.คอม ขอขอบพระคุณล่วงหน้าครับ
                      </Text>
                    </View>
                    <View style={styles.row}>
                      <Text style={[styles.detilsText]}>
                        วันที่เปิดรับสมัคร
                      </Text>
                    </View>
                    <View style={styles.row}>
                      <Text style={styles.contentText}>
                        24 ส.ค 2561 - 26 ก.ย 2561
                      </Text>
                    </View>
                    <View style={styles.row}>
                      <Text style={[styles.detilsText]}>เอกสารเพิ่มเติม</Text>
                    </View>
                    <View style={styles.row}>
                      <Button small success>
                        <Text>ดาวน์โหลด</Text>
                      </Button>
                    </View>
                  </Body>
                </CardItem>
              </Card>
            </View>
          </View>
        </ScrollView>
      </Container>
    )
  }
}
