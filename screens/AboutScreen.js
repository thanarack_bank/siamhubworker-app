import React from "react";
// import { ExpoConfigView } from "@expo/samples";
import { Container } from "native-base";
import HeaderMenu from "../components/Headers/HeaderMenu";

export default class AboutScreen extends React.Component {
  render() {
    return (
      <Container>
        <HeaderMenu {...this.props} title="เกี่ยวกับเรา" />
      </Container>
    );
  }
}
