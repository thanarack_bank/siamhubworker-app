const devMode = true

const devEnv = {
  api: "http://localhost:3010"
}

const propdEnv = {
  api: "http://localhost:3010"
}

const config = {
  reduceSlideMenu: 16,
  api: devMode ? devEnv : propdEnv
}

export default config
