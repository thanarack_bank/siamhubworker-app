import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
};

export const layoutWidthPercentage = (percentage) =>{
  const layoutPercen = (width * percentage) / 100;
  return width - layoutPercen
}