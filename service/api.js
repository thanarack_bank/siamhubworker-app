const api = async data => {
  const { path, method, body, params } = data
  let response = await fetch(path, {
    method,
    body,
    params
  })
  return await response.json()
}

export default api
