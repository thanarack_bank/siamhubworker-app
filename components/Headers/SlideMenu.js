import React, { Component } from "react"
import {
  Platform,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  ImageBackground
} from "react-native"
import { DrawerActions } from "react-navigation"
// import { WebBrowser } from "expo";
import {
  Container,
  Body,
  Icon,
  Header,
  Separator,
  Text,
  ListItem,
  Content,
  Left,
  Right
} from "native-base"

export default class SlideMenu extends Component {
  _onNavigate = screen => {
    const { navigation } = this.props
    navigation.navigate(screen)
    return navigation.dispatch(DrawerActions.closeDrawer())
  }

  _onCategory = (param, name) => {
    const { navigation } = this.props
    navigation.navigate("Category", { id: param, name })
    return navigation.dispatch(DrawerActions.closeDrawer())
  }

  render() {
    //const uri = require("../../assets/images/logo/brand_logo_3.png");
    const { navigation } = this.props
    return (
      <Container>
        <ScrollView>
          <ImageBackground
            source={require("../../assets/images/backgroud_3.jpeg")}
            style={styles.headerBackgroud}
          >
            <Header transparent noShadow style={styles.slideHeader}>
              <Image
                style={styles.brand}
                resizeMode="center"
                source={require("../../assets/images/logo/brand_logo_w_3.png")}
              />
            </Header>
          </ImageBackground>
          <Content>
            {/*<DrawerItems {...this.props}/>*/}
            <ListItem icon last onPress={this._onNavigate.bind(this, "Home")}>
              <Left>
                <Icon type="Ionicons" name="time" style={{ fontSize: 20 }} />
              </Left>
              <Body>
                <Text style={styles.itemText}>งานล่าสุด</Text>
              </Body>
              <Right />
            </ListItem>
            <ListItem
              icon
              last
              onPress={this._onCategory.bind(this, 1, "ประกาศผลสอบ")}
            >
              <Left>
                <Icon
                  type="Ionicons"
                  name="megaphone"
                  style={{ fontSize: 20 }}
                />
              </Left>
              <Body>
                <Text style={styles.itemText}>ประกาศผลสอบ</Text>
              </Body>
              <Right />
            </ListItem>
            {/* <Separator bordered style={styles.separator}>
              <Text style={styles.separator_content}>ประเภทงาน</Text>
            </Separator> */}
            <ListItem
              icon
              last
              onPress={this._onCategory.bind(this, 2, "กำลังรับสมัคร")}
            >
              <Left>
                <Icon
                  type="Ionicons"
                  name="calendar"
                  style={{ fontSize: 20 }}
                />
              </Left>
              <Body>
                <Text style={styles.itemText}>กำลังรับสมัคร</Text>
              </Body>
              <Right />
            </ListItem>
            <ListItem
              icon
              last
              onPress={this._onCategory.bind(this, 3, "งานราชการ")}
            >
              <Left>
                <Icon type="Ionicons" name="school" style={{ fontSize: 20 }} />
              </Left>
              <Body>
                <Text style={styles.itemText}>งานราชการ</Text>
              </Body>
              <Right />
            </ListItem>
            <ListItem
              icon
              last
              onPress={this._onCategory.bind(this, 4, "งานเอกชน")}
            >
              <Left>
                <Icon type="Ionicons" name="school" style={{ fontSize: 20 }} />
              </Left>
              <Body>
                <Text style={styles.itemText}>งานเอกชน</Text>
              </Body>
              <Right />
            </ListItem>
            <Separator bordered style={styles.separator}>
              <Text style={styles.separator_content}>อื่นๆ</Text>
            </Separator>
            {/* <ListItem icon last onPress={() => navigation.navigate("Setting")}>
              <Left>
                <Icon
                  type="Ionicons"
                  name="settings"
                  style={{ fontSize: 20 }}
                />
              </Left>
              <Body>
                <Text style={styles.itemText}>ตั้งค่า</Text>
              </Body>
              <Right />
            </ListItem> */}
            <ListItem icon last onPress={this._onNavigate.bind(this, "About")}>
              <Left>
                <Icon
                  type="Ionicons"
                  name="information-circle"
                  style={{ fontSize: 20 }}
                />
              </Left>
              <Body>
                <Text style={styles.itemText}>เกี่ยวกับเรา</Text>
              </Body>
              <Right />
            </ListItem>
            {/* <ListItem icon last onPress={() => navigation.navigate("Help")}>
              <Left>
                <Icon
                  type="Ionicons"
                  name="help-buoy"
                  style={{ fontSize: 20 }}
                />
              </Left>
              <Body>
                <Text style={styles.itemText}>ช่วยเหลือ</Text>
              </Body>
              <Right />
            </ListItem> */}
          </Content>
        </ScrollView>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  slideHeader: {
    height: "auto",
    backgroundColor: "transparent"
  },
  brand: {
    height: 120,
    width: "80%",
    alignItems: "center"
  },
  headerBackgroud: { width: "100%", height: "auto" },
  itemText: {
    fontSize: 14
  },
  separator_content: {
    fontSize: 14
  },
  separator: {
    marginTop: 10,
    paddingTop: 15,
    backgroundColor: "transparent",
    borderBottomColor: "transparent"
  },
  separatorNoText: {
    marginTop: 10,
    paddingTop: 0,
    height: 0,
    backgroundColor: "transparent",
    borderBottomColor: "transparent"
  }
})
