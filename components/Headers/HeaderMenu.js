import React, { Component } from "React";
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
} from "native-base";
import { StyleSheet } from "react-native";
import { DrawerActions } from "react-navigation";

export default class HeaderMenu extends Component {
  componentDidMount() {
    //
  }

  _openMenu = () => {
    const { navigation } = this.props;
    return navigation.dispatch(DrawerActions.openDrawer());
  };

  render() {
    const { title } = this.props;
    return (
      <Header
        style={styles.header}
        {...this.props}
        androidStatusBarColor={styles.header}
        iosBarStyle="default"
      >
        <Left>
          <Button onPress={this._openMenu} transparent>
            <Icon name="menu" />
          </Button>
        </Left>
        <Body>
          <Title>{title}</Title>
        </Body>
        <Right>
          <Button transparent>
            <Icon name="search" />
          </Button>
        </Right>
      </Header>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#1EA375"
  }
});
