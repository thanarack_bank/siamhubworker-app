export const config = {
  dev: false,
  dev: {
    api_url: "http://localhost:3010/"
  },
  prod: {
    api_url: "http://localhost:3010/"
  }
}

export const api = config.dev ? config.dev : config.prod

export default config
