import React, { Component } from "React"
import {
  List,
  ListItem,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Thumbnail,
  Text,
  View,
  Grid,
  Separator,
  Card,
  CardItem,
  Image
} from "native-base"
import { StyleSheet, TouchableNativeFeedback } from "react-native"
import Colors from "../../constants/Colors"

export default class Post extends Component {
  componentDidMount() {
    //
  }

  render() {
    const { title, index, navigation } = this.props
    const { navigate } = navigation
    // console.log(index === 0)
    return (
      <View>
        <TouchableNativeFeedback onPress={() => navigate("Post", { id: 1 })}>
          <Card style={index === 0 ? styles.card : styles.card2}>
            <CardItem style={styles.headCard}>
              <Left>
                <Thumbnail
                  source={{
                    uri:
                      "https://pbs.twimg.com/profile_images/1014773624039866368/mQWI6QnO_400x400.jpg"
                  }}
                />
                <Body>
                  <Text>กรมชลประทาน</Text>
                  <Text note style={styles.postDate}>
                    20 ส.ค 2562
                  </Text>
                </Body>
              </Left>
            </CardItem>
            <View style={styles.cardBody}>
              <Body>
                <Text style={styles.cardBodyText}>
                  รับสมัครสอบแข่งขันเพื่อบรรจุและแต่งตั้งบุคคลเข้ารับราชการ
                </Text>
              </Body>
            </View>
            <CardItem style={styles.footerCard}>
              <Body style={styles.cardBottom}>
                <View style={styles.labelInfo}>
                  <Icon
                    style={[styles.labelIcon, styles.labelColor]}
                    type="MaterialCommunityIcons"
                    name="map-marker"
                  />
                  <Text style={[styles.labelText, styles.labelColor]}>
                    ยโสธร, ขอนแก่น
                  </Text>
                </View>
                <View style={styles.labelInfo}>
                  <Icon
                    style={[styles.labelIcon, styles.labelColor]}
                    type="MaterialCommunityIcons"
                    name="cash-usd"
                  />
                  <Text style={[styles.labelText, styles.labelColor]}>
                    30,000 - 50,000
                  </Text>
                </View>
                <View style={styles.labelInfo}>
                  <Icon
                    style={[styles.labelIcon, styles.labelColor]}
                    type="MaterialCommunityIcons"
                    name="briefcase"
                  />
                  <Text style={[styles.labelText, styles.labelColor]}>
                    โปรแกรมเมอร์, ไอที
                  </Text>
                </View>
                {/* <View style={styles.labelInfo}>
                  <Icon
                    style={[styles.labelIcon, styles.labelColor]}
                    type="MaterialCommunityIcons"
                    name="calendar-range"
                  />
                  <Text style={[styles.labelText, styles.labelColor]}>
                    24 ส.ค 2561 - 26 ก.ย 2561
                  </Text>
                </View> */}
              </Body>
            </CardItem>
            {/* <View style={styles.readmore}>
            <Button
              full
              iconLeft
              style={styles.readmoreBtn}
              onPress={() => navigate("Post", { id: 1 })}
            >
              <Icon
                style={styles.readmoreBtnIcon}
                type="MaterialCommunityIcons"
                name="open-in-new"
              />
              <Text style={styles.readmoreBtnText}>รายละเอียด</Text>
            </Button>
          </View> */}
          </Card>
        </TouchableNativeFeedback>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#00695C"
  },
  cardBody: {
    margin: 0,
    paddingLeft: 20,
    paddingRight: 20
  },
  postDate: {
    color: "#4D88D7",
    fontSize: 12
  },
  cardBodyText: {
    lineHeight: 24,
    fontWeight: "normal",
    fontSize: 14
  },
  labelColor: {
    color: "#737373"
  },
  card: {
    paddingBottom: 0,
    zIndex: 1,
    borderRadius: 4,
    shadowRadius: 4,
    overflow: "hidden"
  },
  card2: {
    paddingBottom: 0,
    marginTop: 0,
    zIndex: 1,
    borderRadius: 4,
    shadowRadius: 4,
    overflow: "hidden"
  },
  headCard: {
    borderRadius: 4,
    shadowRadius: 4
  },
  footerCard: {
    borderRadius: 4,
    shadowRadius: 4
  },
  labelInfo: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8
  },
  cardBottom: {
    flexDirection: "column"
  },
  labelIcon: {
    fontSize: 16,
    marginRight: 10,
    width: 15
  },
  labelText: {
    fontSize: 12
  },
  readmore: {},
  readmoreBtn: {
    backgroundColor: "#f2f2f2",
    borderTopColor: "#f2f2f2",
    borderTopWidth: 1,
    elevation: 0
  },
  readmoreBtnIcon: {
    color: Colors.tabBar
  },
  readmoreBtnText: {
    fontSize: 14,
    color: Colors.tabBar,
    fontWeight: "bold"
  }
})
