import React, { Component } from "React";
import {
  List,
  ListItem,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Thumbnail,
  Text,
  View,
  Grid,
  Separator
} from "native-base";
import { StyleSheet } from "react-native";

export default class Post extends Component {
  componentDidMount() {
    //
  }

  render() {
    const { title } = this.props;
    return (
      <View>
        <ListItem thumbnail last style={styles.listItem}>
          <Left>
            <Thumbnail
              source={{
                uri:
                  "https://pbs.twimg.com/profile_images/1014773624039866368/mQWI6QnO_400x400.jpg"
              }}
            />
          </Left>
          <Body>
            <Text note numberOfLines={2} style={styles.titlePost}>
              รับสมัครสอบแข่งขันเพื่อบรรจุและแต่งตั้งบุคคลเข้ารับราชการในกรมชลประทาน
            </Text>
          </Body>
          {/*<Right >
          <Icon style={styles.more} type="Ionicons" name="more" /> 
          </Right>*/}
        </ListItem>
        <View style={styles.detailPost}>
          <View style={styles.rowView}>
            <Icon style={styles.textIcon} type="Ionicons" name="flag" />
            <Text note numberOfLines={1} style={styles.textValue}>
              กรมชลประทาน
            </Text>
          </View>
          {/*<View style={styles.rowView}>
            <Icon style={styles.textIcon} type="Ionicons" name="pin" />
            <Text note numberOfLines={1} style={styles.textValue}>
              ยโสธร
            </Text>
          </View>
          <View style={styles.rowView}>
            <Icon style={styles.textIcon} type="Ionicons" name="cash" />
            <Text note numberOfLines={1} style={styles.textValue}>
              30,000 - 50,000
            </Text>
          </View>
          <View style={styles.rowView}>
            <Icon style={styles.textIcon} type="Ionicons" name="bookmarks" />
            <Text note numberOfLines={1} style={styles.textValue}>
              โปรแกรมเมอร์, ไอที
            </Text>
            </View>*/}
          <View style={styles.rowView}>
            <Icon style={styles.textIcon} type="Ionicons" name="time" />
            <Text note numberOfLines={1} style={styles.textValue}>
              24 ส.ค 2561 - 26 ก.ย 2561 
            </Text>
          </View>
        </View>
        <Separator bordered style={styles.separatorNoText} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#00695C"
  },
  listItem: {

  },
  more: {
    flex: 1,
    alignItems: "baseline",
  },
  detailPost: {
    paddingLeft: 20,
    paddingRight: 20
  },
  titlePost: {
    height: 55,
    fontSize: 16,
    color: "#00695C",
    lineHeight: 24
  },
  rowView: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5
  },
  textIcon: {
    fontSize: 16,
    width: 20
  },
  textValue: {
    flex: 1,
    paddingLeft: 5,
    textAlign: "left"
  },
  separatorNoText: {
    marginTop: 10,
    paddingBottom: 0,
    paddingTop: 0,
    height: 0,
    backgroundColor: "transparent",
    borderBottomColor: "transparent"
  }
});
