import React, { Component } from "react"
import { FlatList, StyleSheet, View } from "react-native"
import Post from "./Post"

let mock_data = [{ key: "a" }, { key: "b" }, { key: "c" }, { key: "d" }]

export default class PostList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: mock_data
    }
  }

  _keyExtractor = (item, index) => item.key

  _postRender = item => {
    return (
      <View style={styles.cardPadding}>
        <Post key={+item.index} index={+item.index} {...this.props} />
      </View>
    )
  }

  render() {
    const { data } = this.state
    return (
      <View style={styles.postList}>
        <FlatList
          data={data}
          keyExtractor={this._keyExtractor}
          renderItem={this._postRender}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  postList: {
    flex: 1,
    backgroundColor: "#F1F2F5"
  },
  cardPadding: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 0
  }
})
