import { StyleSheet } from "react-native"

const styles = StyleSheet.create({
  card: {
    paddingBottom: 0,
    marginTop: 0,
    zIndex: 1,
    borderRadius: 4,
    shadowRadius: 4,
    overflow: "hidden"
  },
  cardPadding: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 0
  },
  postScreen: {
    flex: 1,
    backgroundColor: "#F1F2F5",
    marginBottom: 10
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8
  },
  hr: {
    borderWidth: 0.4,
    borderColor: "#737373",
    width: "100%",
    marginTop: 10,
    marginBottom: 10
  },
  headTitle: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: "bold",
    marginBottom: 5
  },
  contentText: {
    fontSize: 14,
    lineHeight: 22
  },
  labelInfo: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8
  },
  labelIcon: {
    fontSize: 16,
    marginRight: 10,
    width: 15
  },
  labelColor: {
    color: "#737373"
  },
  cardBottom: {
    flexDirection: "column"
  },
  labelText: {
    fontSize: 12
  },
  headIcon: {
    color: "#1EA375",
    fontSize: 18,
    marginRight: 30
  },
  headColor: {
    color: "#1EA375"
  },
  detilsText: {
    marginTop: 10,
    fontWeight: "bold",
    fontSize: 14
  },
  banner_ads: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10
  }
})

export default styles
